---
theme: gaia
class: 
  - lead
  - invert
paginate: true
style: |
  section {
    background-color: #611
  }
  h1 {
    color: #faa
  }
---

![bg left:40% 80%](https://coletivo-lcti.com.br/images/logo.png)

# Aplicando para vagas no exterior

Pegadinhas com CVs e cuidados a tomar

LCTI - @automalia
https://codeberg.org/automalia/trampo-na-gringa
https://coletivo-lcti.com.br/

---

#  Disclaimers 

- Minha perspectiva/experiencia pessoal
- Lugares diferentes, regras diferentes
- Vou focar em paises que falam ingles

---

#  O que precisamos pra comecar? 

- Um Curriculo Vitae preenchido (LinkedIn tambem serve)
- Conhecimento basico do idioma estrangeiro
- Conhecimento das politicas de imigracao do(s) pais(es) em questao

---

#  Do que trataremos 

- Comecando (traducao, ferramentas, etc)
- O que vale deixar, o que e melhor tirar
- Evitando pegadinhas e problemas futuros
- Exemplos/templates para formatar

---

#  Comecando: traduzindo CV

Dicas para traduzir texto (Grammarly?)

---

#  O que vale deixar/tirar 

Introducao sobre voce e seus dados
Focar na relevancia (400 experiencias, voluntariados, etc)
Educacao, como colocar
Situacao trabalhista no pais (tem visto? precisa aplicar? precisa de sponsor?)

---

#  Regras locais 

Depende do local, porem:
Sem foto, idade, genero
Pronomes? - pesquisar mais

---

#  Otimizacao para motor de busca (SEO) 

Pesquisar:
Ferramentas pra isso?
Templates prontos?

---

#  Evitando pegadinhas e problemas 

Tudo o que voce disser pode e sera usado contra voce!
Para as conquistas (achievements) listadas nas experiencias, saiba explica-las
Em uma eventual negociacao de salario, saiba o quanto negociar (entenda do real custo de vida, gastos com transporte, aluguel, etc)
Negocie seu deslocamento! Algumas empresas pagam um bonus, pagam voo e acomodacao para incentivar a ida
Conheca o processo de solicitacao de visto, tempo de processamento, documentos necessarios, o que sera necessario a empresa contratante prover


---

#  Exemplos e templates!  

Mostrar exemplos camaradas que aplicam os pontos anteriores
Links para download (repositorio?)


---

#  Agradecimentos  

Contatos (canal no discord?)
Fontes
